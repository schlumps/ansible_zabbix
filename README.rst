******************************
Install Zabbix through Ansible
******************************

Ansible playbook to install and configure Zabbix on localhost. 

Run ``https://bitbucket.org/schlumps/ansible_postgresql`` first to install
PostgreSQL.

This playbook is heavily based on the https://galaxy.ansible.com/jonatasbaldin/zabbix3server/
and https://galaxy.ansible.com/jonatasbaldin/zabbix3agent/ roles.


- Installs Zabbix 3.x.x
- Uses an already installed PostgreSQL database as backend.

::

  # vi server.yml 
  ---

  - hosts: localhost
    roles:
      - server
      - agent
    vars:
      zabbix_version:

  # ansible-playbook -i hosts server.yml

  # vi agent.yml 
  ---

  - hosts: localhost
    roles:
      - server
      - agent
    vars:
      zabbix_version:

  # ansible-playbook -i hosts agent.yml


MJ, 15-Apr-2016
